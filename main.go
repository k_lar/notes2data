package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

type MarkdownData struct {
	Filename    string           `json:"filename"`
	Location    string           `json:"location"`
	Headings    []string         `json:"headings"`
	Subheadings map[int][]string `json:"subheadings"`
	Links       []string         `json:"links"`
	Tags        []string         `json:"tags"`
}

type MarkdownFiles struct {
	Data []MarkdownData `json:"data"`
}

func main() {
	helpFlag := flag.Bool("help", false, "Show help message")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] file1.md file2.md ... or %s [options] /path/to/directory\n", os.Args[0], os.Args[0])
		fmt.Fprintf(os.Stderr, "Options:\n")
		flag.PrintDefaults()
	}
	flag.Parse()

	if *helpFlag {
		flag.Usage()
		return
	}

	files := flag.Args()

	if len(files) == 0 {
		fmt.Println("No files or directories specified.")
		return
	}

	var allMdData []MarkdownData

	for _, fileOrDir := range files {
		info, err := os.Stat(fileOrDir)
		if err != nil {
			log.Fatal(err)
		}

		if info.IsDir() {
			// If the argument is a directory, process all Markdown files within it
			err := filepath.Walk(fileOrDir, func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if info.IsDir() || !strings.HasSuffix(info.Name(), ".md") {
					return nil
				}

				content, err := os.ReadFile(path)
				if err != nil {
					return err
				}

				mdData := ParseMarkdown(string(content))
				mdData.Location = path
				mdData.Filename = info.Name()
				allMdData = append(allMdData, mdData)

				return nil
			})
			if err != nil {
				log.Fatal(err)
			}
		} else if strings.HasSuffix(info.Name(), ".md") {
			// If the argument is a Markdown file, process it
			content, err := os.ReadFile(fileOrDir)
			if err != nil {
				log.Fatal(err)
			}

			mdData := ParseMarkdown(string(content))
			mdData.Location = fileOrDir
			mdData.Filename = info.Name()
			allMdData = append(allMdData, mdData)
		}
	}

	jsonData, err := json.Marshal(MarkdownFiles{Data: allMdData})
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(jsonData))
}

func ParseMarkdown(mdContent string) MarkdownData {
	mdData := MarkdownData{}

	// Extract frontmatter
	frontmatterRegex := regexp.MustCompile(`(?s)^---\s+(.*?)\s+---`)
	frontmatterMatch := frontmatterRegex.FindStringSubmatch(mdContent)
	if len(frontmatterMatch) > 1 {
		frontmatter := frontmatterMatch[1]
		tagsRegex := regexp.MustCompile(`tags:\s*\[([^\]]*)\]`)
		tagsMatch := tagsRegex.FindStringSubmatch(frontmatter)
		if len(tagsMatch) > 1 {
			tags := strings.Split(tagsMatch[1], ",")
			for _, tag := range tags {
				tag = strings.TrimSpace(tag)
				mdData.Tags = append(mdData.Tags, tag)
			}
		}
		// Remove frontmatter from content
		mdContent = strings.Replace(mdContent, frontmatterMatch[0], "", 1)
	}

	// Extract headings
	headingsRegex := regexp.MustCompile(`(?m)^#\s+(.+)`)
	headingsMatches := headingsRegex.FindAllStringSubmatch(mdContent, -1)
	for _, match := range headingsMatches {
		mdData.Headings = append(mdData.Headings, match[1])
	}

	// Extract subheadings
	mdData.Subheadings = make(map[int][]string)
	for level := 2; level <= 6; level++ {
		subheadingsRegex := regexp.MustCompile(fmt.Sprintf(`(?m)^%s\s+(.+)`, strings.Repeat("#", level)))
		subheadingsMatches := subheadingsRegex.FindAllStringSubmatch(mdContent, -1)
		for _, match := range subheadingsMatches {
			mdData.Subheadings[level] = append(mdData.Subheadings[level], match[1])
		}
	}

	// Extract links
	linksRegex := regexp.MustCompile(`\[(.*?)\]\((.*?)\)`)
	linksMatches := linksRegex.FindAllStringSubmatch(mdContent, -1)
	for _, match := range linksMatches {
		mdData.Links = append(mdData.Links, match[2])
	}

	return mdData
}
