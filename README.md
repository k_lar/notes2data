# Notes2data

This tool translates relevant markdown notes data to JSON.

## Features

It ignores all the content, all that this program recognizes as data is:

- Filename
- File location relative to where it was executed (should be used in root of notes folder)
- Headings
- Subheadings (from level 2 to 6)
- Links
- Frontmatter tags

This is the data that gets generated from `test_files/example1.md`:

```json
{
  "data": [
    {
      "filename": "example1.md",
      "location": "test_files/example1.md",
      "headings": [
        "This is a heading"
      ],
      "subheadings": {
        "2": [
          "This is a subheading lvl 2"
        ],
        "3": [
          "This is a subheading lvl 3"
        ],
        "4": [
          "This is a subheading lvl 4"
        ],
        "5": [
          "This is a subheading lvl 5"
        ],
        "6": [
          "This is a subheading lvl 6"
        ]
      },
      "links": [
        "#this-is-a-subheading-lvl-5",
        "#this-is-a-subheading-lvl-1",
        "#this-is-a-subheading-lvl-2",
        "#this-is-a-subheading-lvl-3"
      ],
      "tags": [
        "tag1",
        "tag2"
      ]
    }
  ]
}
```
